#include <iostream>

int main() {
    // unnecessary line feeds for tests
    std::cout 
        << "test" 
        << std::endl;
    return 0;
}
